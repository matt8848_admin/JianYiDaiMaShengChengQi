<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${r"${pageContext.request.contextPath}"}" />
<script>
    var ${clzName}Datagrid;
	var toolbar = [{
		text:'<i class="fa fa-plus-square" aria-hidden="true"></i>增加',
		handler:function(){toadd${clzName}();}
	},{
		text:'<i class="fa fa-edit"></i>修改',
		handler:function(){toupdate${clzName}();}
	},{
		text:'<i class="fa fa-cut" aria-hidden="true"></i>删除',
		handler:function(){todelete${clzName}();}
	},'-',{
		text:'<i class="fa fa-floppy-o" aria-hidden="true"></i>保存',
		handler:function(){alert('save')}
	}];
	$(function(){
		${clzName}Datagrid = $('#${clzName}Datagrid').datagrid({
			filterBtnIconCls:'icon-filter',
			remoteFilter:true
		});
		${clzName}Datagrid.datagrid('enableFilter', [
		   <#list tPropertys as prop>
		    {
			field:'${prop.javaField}',
			type:'textbox',
			op:['contains','equal','notequal','less','greater']
		    }<#if prop_has_next>,</#if>
		   </#list>
		 ]);
		$("#btn-search").bind("click",function(){
			search();
		});
    })
   function search(){
		 ${clzName}Datagrid.datagrid('reload',{
			  /*参数列表*/
	     });
	}
	function toupdate(id){
		var tab = $('#main-tab').tabs('getSelected');
		tab.panel('refresh', "${r"${ctx}"}/${tableName?replace('_', '/' )}/update?_m=init&id="+id);
	}
	function operator(id,status,file){
		var arr = new Array();
		if(status=='1'||status=='4'||status=='7'){
			arr.push("<a class='a-btn' href='javascript:;' onclick='javascript:toupdate(\""+id+"\");'>编辑</a>");
			arr.push("<a class='a-btn' href='javascript:;' onclick='javascript:toupdate(\'"+id+"');'>删除</a>");
		}
		arr.push("<a class='a-btn' target='_blank' href='${r"${ctx}"}/file/"+file+"'>下载</a>");
		return arr.join("");
	}
	function reload(){
		${clzName}Datagrid.datagrid('reload');
	}
</script>
<div class="easyui-layout" style="height:100%;">
	<div data-options="region:'north',border:false">
			 <div id="rpt-condition" class="query-condition">
				<div class="normal-form"> 
					<form role="form" name="${clzName}-search-form" id="${clzName}-search-form" class="easyui-form form form-horizontal" method="post">
							<div class="form-group">
								   <div class="col-sm-2">
									   <a href="javascript:void(0)" class="easyui-linkbutton" data-options="height:'32px',width:'66px'" id="btn-search"><i aria-hidden="true" class="fa fa-search"></i>查询</a>
										<a href="javascript:void(0)" class="easyui-linkbutton" data-options="height:'32px',width:'66px'" id="btn-export"><i aria-hidden="true" class="fa fa-file-excel-o"></i>导出</a>
									</div>
							</div>
					</form>
				</div>
			 </div>
	</div>
	<div data-options="region:'center',border:false">
			<table id="${clzName}Datagrid"  style="width:100%;height:100%"
					data-options="border:true,pageSize:20,rownumbers:true,autoRowHeight:false,singleSelect:true,pagination:true,toolbar:toolbar,rowStyler: function(index,row){return {style:'height:30px'};},url:'${r"${ctx}"}/${tableName?replace('_', '/' )}/search?_m=load',method:'POST'">
				<thead>
					<tr>
					   <#list tPropertys as prop>
					   <th data-options="field:'${prop.javaField}',width:150">${prop.cluComment}</th>
					   </#list>
					</tr>
				</thead>
			</table>
	</div>
</div>

<script type="text/javascript">
      function toadd${clzName}(){
			$('<div></div>').dialog({
				id:'dg-${clzName}-insert',
				title: '<i class="fa fa-windows"></i><span class="dialog-title">${tableComment}信息登记<span>',
				width: 800,
				height:500,
				closed: false,
				cache: false,
				border:false,
				maximizable:true,
				resizable:true,
				href: '${r"${ctx}"}/${tableName?replace('_', '/' )}/insert?_m=init',
				modal: true,
				onClose : function() {
					$(this).dialog('destroy');
				},
				buttons:[{
					id:"btn-${clzName}-save",
					text:'<i class="fa fa-floppy-o" aria-hidden="true"></i>保存',
					handler:function(){regist${clzName}();}
				},{
					id:"btn-${clzName}-back",
					text:'<i class="fa fa-reply" aria-hidden="true"></i>返回',
					handler:function(){$("#dg-${clzName}-insert").dialog('destroy');}
				}]
			});
	  }
	  function toupdate${clzName}(){
		var row = ${clzName}Datagrid.datagrid('getSelected');
		if(row==null){
			$.messager.alert({
				border:false,
				title:'提示',
				msg:'请选择一行数据',
				icon:'warn-ext'
			});
		}else{
			$('<div></div>').dialog({
				id:'dg-${clzName}-update',
				title: '<i class="fa fa-windows"></i><span class="dialog-title">${tableComment}信息登记<span>',
				width: 800,
				height:500,
				closed: false,
				cache: false,
				border:false,
				maximizable:true,
				resizable:true,
				href: '${r"${ctx}"}/${tableName?replace('_', '/' )}/update?_m=init&${keyProperty}='+row.${keyProperty},
				modal: true,
				onClose : function() {
					$(this).dialog('destroy');
				},
				buttons:[{
					id:"btn-${clzName}-update",
					text:'<i class="fa fa-floppy-o" aria-hidden="true"></i>保存',
					handler:function(){update${clzName}();}
				},{
					id:"btn-${clzName}-update-back",
					text:'<i class="fa fa-reply" aria-hidden="true"></i>返回',
					handler:function(){$("#dg-${clzName}-update").dialog('destroy');}
				}]
			});
		}
	  }
	  function todelete${clzName}(){
		    var row = ${clzName}Datagrid.datagrid('getSelected');
			if(row==null){
				$.messager.alert({
					border:false,
					title:'提示',
					msg:'请选择一行数据',
					icon:'warn-ext'
				});
			}else{
				$.messager.confirm({
					border:false,
					title:'提示', 
					msg:'确定要删除?', 
					fn:function(r){
						if (r){
							$.ajax({
					             type: "GET",
					             url: "${r"${ctx}"}/${tableName?replace('_', '/' )}/delete?_m=exec",
					             data: {${keyProperty}:row.${keyProperty}},
					             dataType: "json",
					             success: function(data){
				                        if(data.errcode!=1){
											$.messager.alert({
												border:false,
												title:'提示',
												msg:'已删除',
												icon:'sucess-ext',
												fn:function(){
													reload();
												}
											});
										}else{
											$.messager.alert({
												border:false,
												title:'提示',
												msg:'删除失败',
												icon:'fail-ext',
												fn:function(){
													reload();
												}
											});
											
										}
				                }
					         });
						 }
					}
				});
			}
	  }
</script>
